Wireable
========

Draws pool noodles between nodes in a background canvas!

Fork of [jquery.wireable][] by Aaron Porter.

[jquery.wireable]: https://code.google.com/p/wireable/

# Usage

Make a set of elements wireable nodes:

```js
$('.terminal').wirable(options);
```

Programatically wire/unwire a pair of nodes.

```
$.wire(nodeA, nodeB);
$.unwire(nodeB, nodeC);
```

## Wireable options

_This documentation is incomplete. More information soon!_

### `borderColor`

### `borderWidth`

### `color`

### `container`

Selector or jQuery where the canvas elements will be dumped to.

### `cutClass`

### `drawFunction`

Function that creates an element for dragging. By default, it's
`jQuery.wireable.bezier`.

### `sinkAngle`

Angle at which the wire is "pointing away" from the sink. In degrees.

### `sourceAngle`

Angle at whcih the wire is "pointing away" from the source terminal. In
degrees.

### `target`

jQuery or selector that represents the only allowable nodes that this
node can be wired to. Not that only the existing elements at the
initialization of `wireable` are bound to this method.

### `width`

### `wiredClass`

### `wiringCursor`

## Events

### `wiring`

Occurs when a wire is dropped to be connected. The primary purpose of
this is to abort the wiring with a call to the callback's passed in
event object's  `.preventDefault()` method to prevent the wiring from
occurring.

```js
$('.source').wireable()
  .bind('wiring', function(event) {
    if (!confirm('allow the wire?')) {
      event.preventDefault();
    }
  });
```

Adds extra properties to the event object, `source` and `sink`.

### `unwiring`

Like `wiring`, but triggered when an existing wire is cut.

### `wired`

Triggered when a node has been wired.

Adds extra properties to the event object, `source` and `sink`.

### `unwired`

Triggered when a node has been unwired.

